import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles(theme => ({
  progress: {
    margin: theme.spacing(2),
  },
}));

function Home() {
  const classes = useStyles();
  const [clima, setClima] = useState({})
  const [loading, setLoading] = useState(null)

  useEffect(() => {
    /*async function getFromAPI() {
      const result = await axios.get('api.openweathermap.org/data/2.5/weather?q=London,uk&APPID=847fde79c50651cd74232053251f2325')
      console.log(result);
    }
    getFromAPI()*/
    return () => {};
  }, [])

  const getData = () => {
    async function getFromAPI() {
      setLoading(true)
      const result = await axios.get('https://api.openweathermap.org/data/2.5/weather?q=London,uk&APPID=847fde79c50651cd74232053251f2325')
      console.log(result);
      // Validate result
      if(result.status === 200) {
        setClima({
          name: result.data.name,
          coord: {
            lat: result.data.coord.lat,
            lon: result.data.coord.lon
          },
          humidity: result.data.main.humidity,
          temp: result.data.main.temp
        })
        setLoading(null)
      } else {
        console.log('error: ', result.data.statusText);
      }
    }
    getFromAPI()
  }

  return (
    <div>
      <Button variant="contained" color="primary" onClick={getData}>
        Submit
      </Button>
      {loading && (
        <CircularProgress className={classes.progress} />
      )}
      <pre>
        {JSON.stringify(clima)}
      </pre>
    </div>
  )
}

export default Home
